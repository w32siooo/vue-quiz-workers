import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import { shuffle, mappo } from "../api/shuffle";

Vue.use(Vuex);

export const store = new Vuex.Store({
  //we store all data in our app
  state: {
    categories: [], // trivia categories
    quizLength: 5,
    questions: [],
    question_types: [],
    finalArray: [], //Final array of all answers.
    score: 0,
    currentQuestion: 0,
    correctAnswers:[],
    difficulties: [
      { difficulty: "easy", ID: 0 },
      { difficulty: "medium", ID: 1 },
      { difficulty: "hard", ID: 2 },
    ],
    selectedDifficulty: { difficulty: "easy", ID: 0 },
    currentCategory: 9,
    userAnswers:[],
  },
  //only way to change data in the state is by a mutation,
  //can commit anywhere in add. only trigger synchronous code inside mutation.
  mutations: {
    populateQuiz(state, payload) {
      //reset
      state.questions = [];
      state.finalArray = [];
      state.score = 0;
      state.question_types = [];
      state.currentQuestion=0;
      state.correctAnswers=[];
      state.userAnswers=[]

      //Set the quiz length
      //state.quizLength = payload.results.length;
      //console.log("length is" + payload.results.length);
      //Store the quesitons in store

      payload.results.map((e) => {
        state.question_types.push(e.type);

        //Replace all apostrophes
        var regex = new RegExp("&#039;", "g");
        e.question = e.question.replace(regex, "'");

        //replace all quotes
        var regex2 = new RegExp("&quot;", "g");
        e.question = e.question.replace(regex2, '"');

        state.questions.push(e.question);

        state.correctAnswers.push(e.correct_answer)

      
      });

      state.finalArray = shuffle(mappo(payload));
    },
    loadCategories(state, payload) {
      state.categories = payload.trivia_categories;
    },
    setCurrentCategory: (state, payload) => {
      console.log(payload.name);
      state.currentCategory.name = payload.name;
      state.currentCategory.id = payload.id;
    },
    setCategory: (state, payload) => {
      state.currentCategory = payload;
    },
    numberOfQuestions: (state, payload) => {
      state.quizLength = payload;
    },
    increment(state, payload) {
      state.score += payload;
    },
    setColorCode(state, payload) {
      state.colorCode = payload;
    },
    setDifficulty(state, payload) {
      state.selectedDifficulty = state.difficulties[payload];
      state.selectedDifficulty.id = payload
    },
    currentQuestion(state, payload) {
      state.currentQuestion += payload;
    },
    saveAnswer(state, payload){
      state.userAnswers.push(payload)
    }
  },
  //cant change data in the state, if we want to change data in our state. We commit a mutation in the action!
  //API stuff goes here, "dispatch actions".
  actions: {
    sendAnswer(state, value) {
      if (value.validity) {
        //increment score
        this.commit("increment", 1);
      }
      //increment current question
      this.commit("currentQuestion", 1);

      //save answer
      this.commit("saveAnswer",value.answer)

    },
    startQuiz() {
      const len = this.state.quizLength;
      const dif = this.state.selectedDifficulty.difficulty;
      const catid = this.state.currentCategory;

      axios(
        `https://opentdb.com/api.php?amount=${len}&category=${catid}&difficulty=${dif}`
      ).then((response) => {
        this.commit("populateQuiz", response.data);
      });
    },
    input(payload) {
      console.log(payload.ID);
    },
    loadCategories({ commit }) {
      axios("https://opentdb.com/api_category.php").then((response) => {
        commit("loadCategories", response.data);
      });
    },
  },
});
