import Vue from "vue";
import App from "./App.vue";
import { BootstrapVue } from "bootstrap-vue";
// Styles
import "./main.css";
import 'bootstrap/dist/css/bootstrap.min.css';

// Misc.
import router from "./router";
import { store } from "./store/store";
import VueResource from "vue-resource";

Vue.use(VueResource);
Vue.use(BootstrapVue);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
