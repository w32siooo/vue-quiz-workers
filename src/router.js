import Vue from "vue";
import VueRouter from "vue-router";
import Start from "./components/Start.vue";
import Quiz from "./components/Quiz.vue"
import ScoreScreen from "./components/ScoreScreen"

Vue.use(VueRouter); // Add the Router features to the Vue Object

const routes = [
  {
    path: "/start",
    alias: "/",
    component: Start,
  },
  {
    path: "/quiz",
    component : Quiz,
  },
  {
    path: "/ScoreScreen",
    component : ScoreScreen
  }
];

export default new VueRouter({ routes });
